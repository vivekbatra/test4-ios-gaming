//
//  GameScene.swift
//  BabaIsYou-F19
//
//  Created by Parrot on 2019-10-17.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate
{
    
    
    var baba, flag:SKSpriteNode!
    var canHitFlag = ""
    
    


    override func didMove(to view: SKView)
    {
        self.physicsWorld.contactDelegate = self
        
        self.baba = self.childNode(withName: "baba") as! SKSpriteNode
        self.flag = self.childNode(withName: "flag") as! SKSpriteNode
    }
    
    
    
    
   
    func didBegin(_ contact: SKPhysicsContact)
    {
        
        print("Something collided!")
        let nodeA = contact.bodyA.node
        let nodeB = contact.bodyB.node
        
        if (nodeA == nil || nodeB == nil)
        {
            return
        }
        
        if (nodeA!.name == "isblock" && nodeB!.name == "stopblock" || nodeA!.name == "stopblock" && nodeB!.name == "isblock" || nodeA!.name == "isblock" && nodeB!.name == "wallblock" || nodeA!.name == "wallblock" && nodeB!.name == "isblock")
        {
            print("baba cant cross walls")
            self.baba.physicsBody?.collisionBitMask = 36
        }
        else if(nodeA!.name == "baba" && nodeB!.name == "stopblock" || nodeA!.name == "stopblock" && nodeB!.name == "baba" || nodeA!.name == "baba" && nodeB!.name == "wallblock" || nodeA!.name == "wallblock" && nodeB!.name == "baba")
        {
            print("baba can cross walls")
            self.baba.physicsBody?.collisionBitMask = 32
        }
        
        else if(nodeA!.name == "isblock" && nodeB!.name == "winblock" || nodeA!.name == "winblock" && nodeB!.name == "isblock")
        {
            canHitFlag = "iswin"
//            self.flag.physicsBody?.contactTestBitMask = 2
        }
        
        else if (nodeA!.name == "isblock" && nodeB!.name == "flag" || nodeA!.name == "flag" && nodeB!.name == "isblock")
        {
            canHitFlag = canHitFlag + "flag"
        }
        
        if canHitFlag == "iswinflag" || canHitFlag == "flagiswin"
        {
            self.flag.physicsBody?.contactTestBitMask = 2
        }
        
        if (nodeA!.name == "flag" && nodeB!.name == "baba" || nodeA!.name == "baba" && nodeB!.name == "flag")
        {
            print("WIN")
//            if let scene = SKScene(fileNamed: "Level2") {
//                // Set the scale mode to scale to fit the window
//                scene.scaleMode = .aspectFill
//                SKTransition.flipHorizontal(withDuration: 2.5)
//                // Present the scene
//                view?.presentScene(scene)
        }
            
        

        
    
        
        print("COLLISION DETECTED")
        print("Sprite 1: \(nodeA!.name)")
        print("Sprite 2: \(nodeB!.name)")
        print("------")
    }
    
    
   
    
    
    
    
    override func update(_ currentTime: TimeInterval)
    {
        // Called before each frame is rendered
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
      
        let mouseTouch = touches.first
        if (mouseTouch == nil)
        {
            return
        }
        let location = mouseTouch!.location(in: self)
        let nodeTouched = atPoint(location).name
        
        if (nodeTouched == "upButton")
        {
            self.baba.position.y = self.baba.position.y + 20
        }
        else if (nodeTouched == "downButton")
        {
            self.baba.position.y = self.baba.position.y - 20
        }
        else if (nodeTouched == "leftButton")
        {
            self.baba.position.x = self.baba.position.x - 20
        }
        else if (nodeTouched == "rightButton")
        {
            self.baba.position.x = self.baba.position.x + 20
        }
        
        
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
    {
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
    {
    }
    
    

}
